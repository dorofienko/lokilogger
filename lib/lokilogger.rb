# frozen_string_literal: true

require "lokilogger/formatter"
require "lokilogger/severity"
require "zeitwerk"

Zeitwerk::Loader.new.then do |loader|
  loader.tag = File.basename __FILE__, ".rb"
  loader.push_dir __dir__
  loader.setup
end

# Main namespace.
module Lokilogger
  def self.loader registry = Zeitwerk::Registry
    @loader ||= registry.loaders.find { |loader| loader.tag == File.basename(__FILE__, ".rb") }
  end

  class Logger
    include Severity

    def initialize config
      @client = Lokilogger::Client.new config
      @default_formatter = Formatter.new
    end

    def datetime_format= datetime_format
      @default_formatter.datetime_format = datetime_format
    end

    def datetime_format
      @default_formatter.datetime_format
    end

    attr_accessor :formatter

    def debug(*args, &block)
      message "debug", args, &block
    end

    def debug?
      @client.log_level <= DEBUG
    end

    def debug!
      @client.log_level = DEBUG
    end

    def info(*args, &block)
      message "info", args, &block
    end

    def info?
      @client.log_level <= INFO
    end

    def info!
      @client.log_level = INFO
    end

    def warn(*args, &block)
      message "warn", args, &block
    end

    def warn?
      @client.log_level <= WARN
    end

    def warn!
      @client.log_level = WARN
    end

    def error(*args, &block)
      message "error", args, &block
    end

    def error?
      @client.log_level <= ERROR
    end

    def error!
      @client.log_level = ERROR
    end

    def fatal(*args, &block)
      message "fatal", args, &block
    end

    def fatal?
      @client.log_level <= FATAL
    end

    def fatal!
      @client.log_level = FATAL
    end

    def unknown(*args, &block)
      message "unknown", args, &block
    end

    def unknown?
      @client.log_level <= UNKNOWN
    end

    def unknown!
      @client.log_level = UNKNOWN
    end

    def level= severity
      @client.log_level = Severity.coerce severity
    end

    def level
      @client.log_level
    end

    def message(severity, *args, &block)
      message = block_given? ? yield : args[0][0]
      extra_tags = args[0][1] if args[0].count > 1

      @client.request severity, message, extra_tags
    end
  end
end
