# frozen_string_literal: true

require "async/http/faraday"
require "faraday"
require "lokilogger/error"

module Lokilogger
  # the client
  class Client
    def initialize config
      @config = config
      check_config

      ssl_options = @config[:ssl_options].nil? ? nil : @config[:ssl_options]
      proxy_options = @config[:proxy_options].nil? ? nil : @config[:proxy_options]
      request_options = {timeout: 10, open_timeout: 10, read_timeout: 10, write_timeout: 10}

      Faraday.default_adapter = :async_http
      @conn = Faraday.new url: @config[:url], headers: {"Content-Type" => "application/json"}, ssl: ssl_options, proxy: proxy_options, request: request_options do |builder|
        builder.request :authorization, :basic, @config[:username], @config[:password] if @config[:username] && @config[:password]
      end
    end

    def check_config
      fail ConfigError, "missing/malformed url in config" if !@config[:url] || @config[:url] !~ %r{^(http|https)://}
      fail ConfigError, "missing version in config" unless @config[:version]
      fail ConfigError, "missing tags in config" unless @config[:tags]
      fail ConfigError, "missing log_level in config" unless @config[:log_level]
    end

    def log_level
      @config[:log_level]
    end

    def log_level= log_level
      @config[:log_level] = log_level
    end

    def request severity, message, extra_tags
      now = Time.now
      rational_seconds = now.to_r
      timestamp = (rational_seconds * (10**9)).to_i.to_s

      tags = @config[:tags]
      tags = tags.merge extra_tags unless extra_tags.nil?
      tags = tags.merge({severity:})

      Async do
        @conn.post "/loki/api/#{@config[:version]}/push" do |req|
          body = {streams: [{stream: tags, values: [[timestamp, message]]}]}.to_json

          req.body = body
        end
      rescue Faraday::Error => error
        puts error.response[:status]
        puts error.response[:body]
      end
    end
  end
end
