# frozen_string_literal: true

module Lokilogger
  # the default error
  class Error < StandardError
  end

  # something is wrong with the config
  class ConfigError < Error
  end
end
