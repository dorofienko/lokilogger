# frozen_string_literal: true

# Main namespace.
module Lokilogger
  Version = "1.1.1"
end
