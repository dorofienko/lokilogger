# frozen_string_literal: true

lib = File.expand_path "lib", __dir__
$LOAD_PATH.unshift lib unless $LOAD_PATH.include? lib
require "lokilogger/version"

Gem::Specification.new do |spec|
  spec.name = "lokilogger"
  spec.version = Lokilogger::Version
  spec.authors = ["Nils Bartels"]
  spec.email = ["rubygems@nils.bartels.xyz"]
  spec.homepage = "https://gitlab.com/sukunai/lokilogger"
  spec.summary = "Log asynchronously to Grafana Loki"
  spec.license = "MIT"

  spec.metadata = {"label" => "lokilogger", "rubygems_mfa_required" => "true"}

  spec.required_ruby_version = "~> 3.2"
  spec.add_dependency "async", "~> 2.6", ">= 2.6.5"
  spec.add_dependency "async-http-faraday", "~> 0.12.0"
  spec.add_dependency "faraday", "~> 2.7", ">= 2.7.12"
  spec.add_dependency "refinements", "~> 11.0"
  spec.add_dependency "zeitwerk", "~> 2.6"

  spec.extra_rdoc_files = Dir["README*", "LICENSE*"]
  spec.files = Dir["*.gemspec", "lib/**/*"]
end
