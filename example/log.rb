#!/usr/bin/env ruby
# frozen_string_literal: true

require "bundler/setup"
Bundler.require :tools

anfang = Time.now

require "lokilogger"

url = ENV.fetch "URL", nil
username = ENV.fetch "USERNAME", nil
password = ENV.fetch "PASSWORD", nil

logger = Lokilogger::Logger.new({url:, log_level: 0, version: "v1", username:, password:, tags: {foo: "bar"}})

Async do
  einers = Time.now
  logger.debug("some debug", {useless: "tag"})
  puts Time.now - einers
  einers = Time.now
  logger.info("an info", {useless: "tag"})
  puts Time.now - einers
  einers = Time.now
  logger.warn("a warning", {useless: "tag"})
  puts Time.now - einers
  einers = Time.now
  logger.error("an error", {useless: "tag"})
  puts Time.now - einers
  einers = Time.now
  logger.fatal("a fatal", {useless: "tag"})
  puts Time.now - einers
  einers = Time.now
  logger.unknown("into the unknown", {useless: "tag"})
  puts Time.now - einers
end

puts logger.level
logger.level = 3
puts logger.level
puts logger.unknown?
puts logger.debug?

puts Time.now - anfang
